package io.github.t3m8ch.tovarishchxi.services

import io.github.t3m8ch.tovarishchxi.dto.CreateUpdateUserDTO
import io.github.t3m8ch.tovarishchxi.dto.UserDTO

interface UsersService {
    fun updateOrCreate(dto: CreateUpdateUserDTO): UserDTO
}
