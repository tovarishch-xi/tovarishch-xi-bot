package io.github.t3m8ch.tovarishchxi.services.impl

import io.github.t3m8ch.tovarishchxi.dto.CreateUpdateUserDTO
import io.github.t3m8ch.tovarishchxi.dto.UserDTO
import io.github.t3m8ch.tovarishchxi.models.UserModel
import io.github.t3m8ch.tovarishchxi.repositories.UsersRepository
import io.github.t3m8ch.tovarishchxi.services.UsersService
import io.github.t3m8ch.tovarishchxi.utils.mapToDTO
import io.github.t3m8ch.tovarishchxi.utils.mapToModel
import org.springframework.stereotype.Service

@Service
class UsersServiceImpl(private val usersRepository: UsersRepository) : UsersService {
    override fun updateOrCreate(dto: CreateUpdateUserDTO): UserDTO {
        val existingUser = usersRepository.findOrNullByTelegramId(dto.telegramId)
        if (existingUser != null) {
            return updateExistingUser(existingUser, dto)
        }
        return usersRepository.save(dto.mapToModel()).mapToDTO()
    }

    private fun updateExistingUser(existingUser: UserModel, dto: CreateUpdateUserDTO): UserDTO {
        existingUser.firstName = dto.firstName
        existingUser.lastName = dto.lastName
        existingUser.username = dto.username

        return usersRepository.save(existingUser).mapToDTO()
    }
}
