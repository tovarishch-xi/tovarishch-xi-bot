package io.github.t3m8ch.tovarishchxi.repositories

import io.github.t3m8ch.tovarishchxi.models.UserModel
import org.springframework.data.repository.CrudRepository

interface UsersRepository : CrudRepository<UserModel, Long> {
    fun findOrNullByTelegramId(telegramId: Long): UserModel?
}
