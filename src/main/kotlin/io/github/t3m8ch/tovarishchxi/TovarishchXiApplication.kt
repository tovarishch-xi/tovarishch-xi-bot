package io.github.t3m8ch.tovarishchxi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TovarishchXiApplication

fun main(args: Array<String>) {
    runApplication<TovarishchXiApplication>(*args)
}
