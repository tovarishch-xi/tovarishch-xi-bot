package io.github.t3m8ch.tovarishchxi.utils

import io.github.t3m8ch.tovarishchxi.dto.CreateUpdateUserDTO
import io.github.t3m8ch.tovarishchxi.dto.UserDTO
import io.github.t3m8ch.tovarishchxi.models.UserModel

fun CreateUpdateUserDTO.mapToModel() = UserModel(
    telegramId = telegramId,
    firstName = firstName,
    lastName = lastName,
    username = username,
)

fun UserModel.mapToDTO() = UserDTO(
    id!!,
    telegramId!!,
    createdAt!!,
    updatedAt!!,
    firstName!!,
    lastName,
    username
)
