package io.github.t3m8ch.tovarishchxi.context

import io.github.t3m8ch.tovarishchxi.dto.UserDTO
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.bots.AbsSender

data class Context(
    val update: Update,
    val sender: AbsSender,
    val user: UserDTO,
)
