package io.github.t3m8ch.tovarishchxi.handlers

import io.github.t3m8ch.tovarishchxi.context.Context

abstract class Handler(val filter: (Context) -> Boolean) {
    abstract fun handle(context: Context)
}