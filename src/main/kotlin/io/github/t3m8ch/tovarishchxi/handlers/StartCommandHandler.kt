package io.github.t3m8ch.tovarishchxi.handlers

import io.github.t3m8ch.tovarishchxi.context.Context
import org.springframework.stereotype.Component
import org.telegram.abilitybots.api.util.AbilityUtils.getChatId
import org.telegram.telegrambots.meta.api.methods.send.SendMessage

@Component
class StartCommandHandler : Handler({ it.update.hasMessage() && it.update.message?.text == "/start" }) {
    override fun handle(context: Context) {
        val method = SendMessage.builder()
            .text("Привет! Я Товарищ Хи!")
            .chatId(getChatId(context.update))
            .build()

        println(context.user)
        context.sender.execute(method)
    }
}
