package io.github.t3m8ch.tovarishchxi.bot

import io.github.t3m8ch.tovarishchxi.config.TelegramBotProperties
import io.github.t3m8ch.tovarishchxi.context.Context
import io.github.t3m8ch.tovarishchxi.dto.CreateUpdateUserDTO
import io.github.t3m8ch.tovarishchxi.dto.UserDTO
import io.github.t3m8ch.tovarishchxi.handlers.Handler
import io.github.t3m8ch.tovarishchxi.services.UsersService
import org.springframework.stereotype.Component
import org.telegram.abilitybots.api.util.AbilityUtils.getUser
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.objects.Update

@Component
class TelegramBot(
    private val props: TelegramBotProperties,
    private val handlers: List<Handler>,
    private val usersService: UsersService,
) : TelegramLongPollingBot() {
    override fun getBotToken() = props.token
    override fun getBotUsername() = props.username

    override fun onUpdateReceived(update: Update) {
        val user = usersService.updateOrCreate(CreateUpdateUserDTO.fromTelegramUpdate(update))
        val context = createContextFromUpdate(update, user)
        handle(context)
    }

    private fun handle(context: Context) {
        val handler = handlers.firstOrNull { it.filter(context) } ?: return
        handler.handle(context)
    }

    private fun createContextFromUpdate(update: Update, user: UserDTO) = Context(
        update = update,
        sender = this,
        user = user,
    )
}

private fun CreateUpdateUserDTO.Companion.fromTelegramUpdate(update: Update): CreateUpdateUserDTO {
    val telegramUser = getUser(update)
    return CreateUpdateUserDTO(
        telegramId = telegramUser.id,
        firstName = telegramUser.firstName,
        lastName = telegramUser.lastName,
        username = telegramUser.userName,
    )
}
