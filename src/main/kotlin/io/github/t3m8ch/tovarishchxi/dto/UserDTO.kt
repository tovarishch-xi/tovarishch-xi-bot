package io.github.t3m8ch.tovarishchxi.dto

import java.time.ZonedDateTime

data class UserDTO(
    val id: Long,
    val telegramId: Long,
    val createdAt: ZonedDateTime,
    val updatedAt: ZonedDateTime,
    val firstName: String,
    val lastName: String? = null,
    val username: String? = null,
)