package io.github.t3m8ch.tovarishchxi.dto

data class CreateUpdateUserDTO(
    val telegramId: Long,
    val firstName: String,
    val lastName: String? = null,
    val username: String? = null,
) {
    companion object
}
