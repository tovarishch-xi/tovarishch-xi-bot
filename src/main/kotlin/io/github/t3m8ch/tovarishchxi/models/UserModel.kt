package io.github.t3m8ch.tovarishchxi.models

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Table(name = "user_account")
class UserModel(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,

    @Column(name = "telegram_id", nullable = false, unique = true)
    var telegramId: Long? = null,

    @CreationTimestamp
    @Column(name = "created_at", nullable = false)
    var createdAt: ZonedDateTime? = null,

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    var updatedAt: ZonedDateTime? = null,

    @Column(name = "first_name", nullable = false)
    var firstName: String? = null,

    @Column(name = "last_name", nullable = true)
    var lastName: String? = null,

    @Column(name = "username", nullable = true)
    var username: String? = null,
)
