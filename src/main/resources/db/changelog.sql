-- liquibase formatted sql

-- changeset t3m8ch:1
CREATE TABLE user_account(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    telegram_id BIGINT UNIQUE NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255),
    username VARCHAR(255)
);
-- rollback DROP TABLE user_account;
