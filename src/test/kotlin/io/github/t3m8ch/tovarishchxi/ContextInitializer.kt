package io.github.t3m8ch.tovarishchxi

import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.testcontainers.containers.MariaDBContainer
import org.testcontainers.utility.DockerImageName

class ContextInitializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
    override fun initialize(applicationContext: ConfigurableApplicationContext) {
        TestPropertyValues.of(
            "spring.datasource.url=${mariaDBContainer.jdbcUrl}",
            "spring.datasource.username=${mariaDBContainer.username}",
            "spring.datasource.password=${mariaDBContainer.password}",
        ).applyTo(applicationContext.environment)
    }

    companion object {
        private val mariaDBContainer = MariaDBContainer(DockerImageName.parse("mariadb"))
            .withDatabaseName("tovarishch-xi-test-db")
            .withUsername("sa")
            .withPassword("sa")

        init {
            mariaDBContainer.start()
        }
    }
}
