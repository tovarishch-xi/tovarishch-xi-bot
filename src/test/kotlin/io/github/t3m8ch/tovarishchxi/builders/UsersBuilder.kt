package io.github.t3m8ch.tovarishchxi.builders

import io.github.t3m8ch.tovarishchxi.models.UserModel

fun users(configure: UsersBuilder.() -> Unit): List<UserModel> {
    val builder = UsersBuilder()
    builder.configure()
    return builder.build()
}

class UsersBuilder : TestDataBuilder<UserThatElement, UserModel>({
    createThatElement = { UserThatElement() }
    mapThatElementToObject = { it, i ->
        UserModel(
            telegramId = it.telegramId ?: i,
            firstName = it.firstName,
            lastName = it.lastName,
            username = it.username,
        )
    }
})

class UserThatElement : ThatElement() {
    var telegramId: Long? = null
        private set

    var firstName: String = ""
        private set

    var lastName: String? = null
        private set

    var username: String? = null
        private set

    fun hasTelegramId(telegramId: Long) {
        this.telegramId = telegramId
    }

    fun hasFirstName(firstName: String) {
        this.firstName = firstName
    }

    fun hasLastName() {
        this.lastName = lastName
    }

    fun hasUsername() {
        this.username = username
    }
}
